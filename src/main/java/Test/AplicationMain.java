package Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.UsuariosDAO;
import modelo.Usuarios;

public class AplicationMain {
    
    public static void main (String[] args)  throws SQLException{
    
        UsuariosDAO usuarioDao;
        
//        String jdbcURL = "jdbc:mysql://localhost:3306/andinos?useSSL=false";
//        String jdbcUsername = "root";
//        String jdbcPassword = "lamisma";
        
        String jdbcURL = "jdbc:mysql://minticloud.uis.edu.co/c3s5grupo2?useSSL=false";
        String jdbcUsername = "c3s5grupo2";
        String jdbcPassword = "7lvoTJlJ";
         
                
        System.out.println("################ INSERTAR USUARIO #######################");
        
        // INSERTAR REGISTRO TABLA USER
        try {
            usuarioDao = new UsuariosDAO(jdbcURL, jdbcUsername, jdbcPassword);
            
            Usuarios usuario = new Usuarios("Juan", "Suarez", "3133932780", "juan.suarez@gmail.com", "122327", 1, 1, "123456");
                    
            boolean rowInserted = usuarioDao.insertarUsuarios(usuario);
               
            if (rowInserted = true){
                System.out.println("El usuario "+ usuario.getName() + " "+ usuario.getLastname() + " se ha creado EXITOSAMENTE");
            } else {
                System.out.println("Usuario no creado");
            }

        } catch (Exception e) {
            System.out.println("ERROR: El usuario no pudo ser creado, ya existe!!");
        }
        
        System.out.println("################ LOGIN USUARIO #######################");
        
        // CONSULTAR USUARIO - UTILIZAR EN LOGIN
        try {
            usuarioDao = new UsuariosDAO(jdbcURL, jdbcUsername, jdbcPassword);
            Usuarios usuario = usuarioDao.Login("79729511", "123456");
            
            if (usuario.getName() != null){
                System.out.println("Bienvenido "+ usuario.getName() + " "+ usuario.getLastname());
            } else {
                System.out.println("ERROR: Credenciales incorrectas");
            }

        } catch (Exception e) {
            System.out.println("ERROR: Credenciales incorrectas");
        }
        
        System.out.println("################ CONSULTAR REGISTRO EN BD  #######################");
        
         // CONSULTAR REGISTRO TABLA USER POR NUMERO DE CEDULA
        try {
            usuarioDao = new UsuariosDAO(jdbcURL, jdbcUsername, jdbcPassword);
            
            Usuarios usuario = new Usuarios();
            
            usuario = usuarioDao.buscarUsuarios("1023391442");
             
           if (usuario.getName() != null){
                System.out.println(usuario.getName());
                System.out.println(usuario.getLastname());
            } else {
                System.out.println("Usuario no EXISTE");
            } 
        } catch (Exception e) {
            System.out.println("ERROR: Usuario no encontrado");
        }
        
        System.out.println("################ ELIMINAR USUARIO #######################");
        
        // ELIMINAR REGISTRO TABLA USER POR NUMERO DE CEDULA
        try {
            usuarioDao = new UsuariosDAO(jdbcURL, jdbcUsername, jdbcPassword);
                        
            boolean resultado = usuarioDao.eliminarUsuarios("1023391443");
            
            if (resultado == true){
                System.out.println("El usuario se ha Eliminado exitosamente");
            } else {
                System.out.println("ERROR: Usuario no encontrado");
            }
           

        } catch (Exception e) {
            System.out.println("ERROR: Usuario no existe");
        }
        
        System.out.println("################ ACTUALIZAR USUARIO #######################");
        
         // ACTUALIZAR REGISTRO TABLA USER POR NUMERO DE CEDULA
        try {
            usuarioDao = new UsuariosDAO(jdbcURL, jdbcUsername, jdbcPassword);
            
            boolean actualizado = false;
            
            actualizado = usuarioDao.actualizarUsuarios("Maria Paula", "Suarez Figueroa", "125445454");
             
           if (actualizado == true){
               System.out.println("Usuario Actualizado: ");                
            } else {
                System.out.println("ERROR: Usuario no se pudo actualizar");
            } 
        } catch (Exception e) {
            System.out.println("ERROR: El usuario no se ha actualizado");
        }
        
        
    }  
}  
    
        
        
            