package config;

import java.sql.Connection; 
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.mysql.jdbc.Driver;

public class Conexion {
    public Connection conn;
    private String jdbcURL;
    private String jdbcUsername;
    private String jdbcPassword;
    
    public Conexion(String jdbcURL, String jdbcUsername, String jdbcPassword){
            this.jdbcURL = jdbcURL;
            this.jdbcUsername = jdbcUsername;
            this.jdbcPassword = jdbcPassword;
    }
    
    public void conectar() throws SQLException {
            
        conn = null;
            
            if ( conn == null || conn.isClosed())
            {
                try {
                   
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    conn = (Connection) DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
                    //System.out.println("bien!! Conectado BD");
                    
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
                    //System.out.println("Paila!! no se pudo conectar");
                }
            }
    }
    
    public void desconectar() throws SQLException {
         if ( conn != null || !conn.isClosed())
            {
                conn.close();
                //System.out.println("DESconectado BD");
            }
        
    }
    
    public Connection getConnection(){
        return conn;
    }    
}