/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author mauricio
 */
public class Usuarios {
    
    String name, lastname, mobile, email, identification, password;
    int document_type, status;

    public Usuarios(String name, String lastname, String mobile, String email, String identification, int document_type, int status, String password) {
        this.name = name;
        this.lastname = lastname;
        this.mobile = mobile;
        this.email = email;
        this.identification = identification;
        this.document_type = document_type;
        this.status = status;
        this.password = password;
    }

    public Usuarios() {
    }
    
    
    public Usuarios(String identification, String name, String lastname, int status){
        this.identification = identification;
        this.name = name;
        this.lastname = lastname;
        this.status = status;
    }
    
    public Usuarios(String identification, String password){
        this.identification = identification;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public int getDocument_type() {
        return document_type;
    }

    public void setDocument_type(int document_type) {
        this.document_type = document_type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
}
