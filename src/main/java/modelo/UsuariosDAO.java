/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import config.Conexion;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mauricio
 */
public class UsuariosDAO implements LoginInterface {

    //private ConnectionManager con;
    private Connection connection;
    private Conexion con;
    
    public UsuariosDAO(String jdbcURL, String jdbcUsername, String jdbcPassword){
        
        con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword );

    };
       
    public boolean insertarUsuarios(Usuarios usuario) throws SQLException {
        
        String sql = "INSERT INTO user(name, lastname, mobile, email, identification, document_type, status, password) VALUES (?,?,?,?,?,?,?,?)";
        con.conectar();
        connection = con.getConnection();
        
        PreparedStatement statement = connection.prepareStatement(sql);
        
        statement.setString(1, usuario.getName());
        statement.setString(2, usuario.getLastname());
        statement.setString(3, usuario.getMobile());
        statement.setString(4, usuario.getEmail());
        statement.setString(5, usuario.getIdentification());
        statement.setInt(6, usuario.getDocument_type());
        statement.setInt(7, usuario.getStatus());
        statement.setString(8, usuario.getPassword());
         
        boolean rowInserted;
        
        if ( rowInserted = statement.executeUpdate() > 0) {
            //System.out.println("Creación exitosa");
        } else {
            //System.out.println("ERROR: Usuario ya existe");
            rowInserted = false;
        }
        
        statement.close();
        con.desconectar();
        
    return rowInserted;
}
    
    public Usuarios buscarUsuarios(String identificacion ) throws SQLException {
        
        String sql = "SELECT identification, name, lastname, status FROM user where identification =?";
        con.conectar();
        connection = con.getConnection();
        
        PreparedStatement ps;
        ResultSet rs;  
        Usuarios usuario = null;
                
        try {

            PreparedStatement statement = connection.prepareStatement(sql);
        
            statement.setString(1, identificacion);
        
            rs = statement.executeQuery();
            
            while (rs.next()){
                
                usuario = new Usuarios();
                usuario.setName(rs.getString("name"));
                usuario.setLastname(rs.getString("lastname"));
                
            }
            rs.close();
            con.desconectar();
            return usuario;
                       
            
        } catch (Exception e) {
            System.out.println("Error: "+ e.toString());
            return null;
        }
        
    }
        
    public Usuarios Login(String login, String pwd) throws SQLException{
    
        Usuarios usuario = new Usuarios();
        
        String sql = "SELECT identification, name, lastname, status FROM user where identification = ? and password = ?";
        
        con.conectar();
        connection = con.getConnection();
     
        PreparedStatement statement = connection.prepareStatement(sql);
        
        statement.setString(1, login);
        statement.setString(2, pwd);
        
        ResultSet rs = statement.executeQuery();
            
            while (rs.next()){
                
                usuario = new Usuarios();
                //System.out.println("Login Usuarios" + rs.getNString("name"));
                usuario.setName(rs.getString("name"));
                usuario.setLastname(rs.getString("lastname"));
                
            }
            rs.close();
            con.desconectar();
            return usuario;
    }
    
    public boolean eliminarUsuarios(String identificacion ) throws SQLException {
        
        String sql = "DELETE FROM user where identification ="+identificacion;
        
        try {
            con.conectar();
            connection = con.getConnection();

            boolean rowDeleted;
            Statement statement = connection.createStatement();
            
//            try (Statement statement = connection.createStatement()) {
//                statement.execute(sql);
//                rowDeleted = true;
//            }catch (Exception er){
//                System.out.println("Error: "+ er.toString());
//                return false;
//            }

            if ( !(rowDeleted = statement.execute(sql)) ) {
                //System.out.println("ERROR: Usuario no encontrado");
                rowDeleted = false;
            } else {
                //System.out.println("ELIMINACION exitosa");
                rowDeleted = true;
            }
            
            con.desconectar();
            return rowDeleted;                       
        
        } catch (Exception e) {
            System.out.println("Error: "+ e.toString());
            return false;
        }
        
    }
    
    public boolean actualizarUsuarios(String name, String lastname, String identification ) throws SQLException {
        
        String sql = "UPDATE user SET name = ?, lastname = ? where identification =?";
        
        try {
            
            con.conectar();
            connection = con.getConnection();
            
            boolean actualizado = false;
            
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setString(1, name);
            statement.setString(2, lastname);
            statement.setString(3, identification);
            
            int res = statement.executeUpdate();
            
            if (res > 0){
                actualizado = true; 
            } else {
                actualizado = false;
            }
            
            con.desconectar();
            return actualizado;

        } catch (Exception e) {
            System.out.println("Error: "+ e.toString());
            return false;
        }
    }
}
