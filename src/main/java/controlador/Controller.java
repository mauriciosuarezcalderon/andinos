package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import modelo.UsuariosDAO;
import modelo.Usuarios;

@WebServlet("/Controller")
public class Controller extends HttpServlet {
    
    private static final long serialVersionUID = 1L;

    String jdbcURL = "jdbc:mysql://localhost:3306/andinos?useSSL=false";
    String jdbcUsername = "root";
    String jdbcPassword = "lamisma";
    
//      String jdbcURL = "jdbc:mysql://minticloud.uis.edu.co/c3s5grupo2?useSSL=false";
//      String jdbcUsername = "c3s5grupo2";
//      String jdbcPassword = "7lvoTJlJ";
    
    UsuariosDAO usuarioDao = new UsuariosDAO(jdbcURL, jdbcUsername, jdbcPassword);
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String usuario = request.getParameter("usuario");
        String password = request.getParameter("clave");
        
        Usuarios user = null;
        
        try {
            user = usuarioDao.Login(usuario, password);
        } catch (SQLException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        if(user.getName()!=null){
            try {
                request.setAttribute("successMessage", "Se registró el usuario exitosamente");
                request.getRequestDispatcher("Ventas/ventas.jsp").forward(request, response);
                return;
            } catch (ServletException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
            } else {
                System.out.println("Usuario no encontrado");
                response.sendRedirect(request.getContextPath() + "index.jsp");
                return;
            }
    }
}

