<%-- 
    Document   : ventas
    Created on : 16/09/2021, 8:45:48 p. m.
    Author     : mauricio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registra tus ventas</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    </head>
    <body>        
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a style="color: blue" class="navbar-brand" href="#">Inicio</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                      <li class="nav-item">
                        <a style="color: blue" class="nav-link active" aria-current="page" href="#">Ventas</a>
                      </li>
                      <li class="nav-item">
                        <a style="color: blue" class="nav-link" href="#">Inventarios</a>
                      </li>
                      <li class="nav-item">
                        <a style="color: blue" class="nav-link" href="#">Flujo de caja</a>
                      </li>
                    </ul>
                </div>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                      Cerrar sesión
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><center><a><img src="../img/usuario.jpg" height="80" width="80"></a></center></li>
                      <li><a class="dropdown-item text-center" href="#">Usuario</a></li>
                      <li><a href="index.jsp" class="dropdown-item text-center" >Salir</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <br><br>
        <div class="container">
          <div class="card">
            <h5 class="card-header">Andinos - Registra tus ventas</h5>
            <div class="card-body">
              <h5 class="card-title">Cliente</h5>
              <p class="card-text">Fideliza a cada uno de tus clientes y haz que se multipliquen las ventas día a día.</p>  
                <div class="row">
                  <div class="col-1"></div>
                  <div class="col-5">
                      <input type="text" class="form-control" id="cedula" placeholder="Cedula cliente" />
                  </div>
                  <div class="col-5">
                      <input type="text" class="form-control"  name="nombre" placeholder="Nombre cliente" />
                  </div>
                  <div class="col-1"></div>
                </div>
              <br>
                <div class="row">
                  <div class="col-1"></div>
                  <div class="col-5">
                      <input type="text" class="form-control" name="celular" placeholder="Celular" /><!-- comment -->
                  </div>
                  <div class="col-5">
                      <input type="text" class="form-control" name="correo" placeholder="Correo" /><!-- comment -->
                  </div>
                  <div class="col-1"></div>
                </div>
              <br>
              <div class="row">
              <div class="col-5"></div>              
              <div class="col-2"><a href="#" class="btn btn-primary">Buscar</a></div>              
              <div class="col-5"></div>              
            </div>
          </div>
        </div>
            <br><br>
        <div>
            <div class="row">
                <div class="col-1"></div>
                <div class="col-6 ">
                    <table class="table table-striped">
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Valor</th>
                        </tr>
                        <tr>
                            <td>Papas fritas</td>
                            <td>10</td>
                            <td>30000</td>
                        </tr>
                        <tr>
                            <td>Leche alqueria</td>
                            <td>2</td>
                            <td>7000</td>
                        </tr>
                        <tr>
                            <td>Gaseosa Coca-cola</td>
                            <td>5</td>
                            <td>15000</td>
                        </tr><!-- comment -->
                        <tr>
                            <td>Pan tajado sandwich</td>
                            <td>6</td>
                            <td>19500</td>
                        </tr>
                    </table>
                </div>
                <div class="col-4">
                    <div class="row">
                        <div col>
                            <input type="text" class="form-control" name="producto" placeholder="Buscar producto" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-2">
                          <a href"#"><img src="../img/leche.jpg" width="100px" height="100px"></a>
                         </div>
                        <div class="col-2">
                            <a href"#"><img src="../img/pan.jpg" width="100px" height="100px"></a>
                        </div><!-- comment -->
                        <div class="col-2">
                          <a href"#"><img src="../img/huevos.jpg" width="100px" height="100px"></a>
                         </div>
                        <div class="col-2">
                          <a href"#"><img src="../img/aceite.jpg" width="100px" height="100px"></a>
                         </div>
                        <div class="col-2"></div>
                    </div>
                </div>
            </div>
         </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    </body>
</html>
