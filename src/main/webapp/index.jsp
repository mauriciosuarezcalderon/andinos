<%-- 
    Document   : index
    Created on : 17/09/2021, 6:34:04 a. m.
    Author     : mauricio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" href="css/master.css">
        <title>Andinos tienda virtual</title>
    </head>
    <body>
        <div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1>Bienvenido a MiTiendaVirtual Andinos</h1>
			</div>
			<div class="login-form">
                            <form action="Controller" method="post">
                                <div class="form-group">
                                    <input type="text" class="login-field" name ="usuario" placeholder="Digite su número de documento" >
                                    <label class="login-field-icon fui-user" for="Nombre-Ingreso"></label>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="login-field" name="clave" placeholder="Contraseña">
                                    <label class="login-field-icon fui-lock" for="Contraseña"></label>
                                </div>
                                <div><input class="btn btn-primary btn-large btn-block" type="submit" value="Ingresar"></div>
                            </form>
                            <a class="login-link" href="#">Olvidaste tu contraseña?</a>
                        </div>
		</div>
        </div>
    </body>
</html>