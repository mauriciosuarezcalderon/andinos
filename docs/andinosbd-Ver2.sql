-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-09-2021 a las 22:28:35
-- Versión del servidor: 8.0.26-0ubuntu0.21.04.3
-- Versión de PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `andinos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `idclient` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `status` int DEFAULT NULL,
  `document_type` int NOT NULL,
  `identification` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `details_sale`
--

CREATE TABLE `details_sale` (
  `iddetails_sale` int NOT NULL,
  `sale_id` int NOT NULL,
  `product_id` int NOT NULL,
  `amount` double NOT NULL,
  `quantity` int NOT NULL,
  `iva` double NOT NULL,
  `total` double NOT NULL,
  `sale_idsale` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `document_type`
--

CREATE TABLE `document_type` (
  `idtable1` int NOT NULL,
  `document_type` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `document_type`
--

INSERT INTO `document_type` (`idtable1`, `document_type`, `created_at`, `updated_at`) VALUES
(1, 'Cedula Ciudadania', '2021-09-16 02:21:49', '2021-09-16 02:21:49');
INSERT INTO `document_type` (`idtable1`, `document_type`, `created_at`, `updated_at`) VALUES
(2, 'Cedula Extranjeria', '2021-09-16 02:21:49', '2021-09-16 02:21:49');
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `idproduct` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `code` varchar(45) NOT NULL,
  `type_id` int NOT NULL,
  `provider_id` int NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`idproduct`, `name`, `code`, `type_id`, `provider_id`,`status`, `created_at`, `updated_at`) VALUES
(1, 'Huevo AA', '10001', 1, 1, 1,'2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(2, 'Huevo AAA', '10002', 1, 1, 1,'2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(3, 'Huevo campesino AA', '10003', 1, 1, 1,'2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(4, 'Huevo campesino AAA', '10004', 1, 1, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(5, 'Leche 1100', '21008', 2, 2, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(6, 'Leche 1300', '21009', 2, 2, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(7, 'Queso campesino', '21010', 2, 2, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(8, 'Que doble crema', '21011', 2, 2, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(9, 'Yogourt frutas', '21012', 2, 2,1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(10, 'Kumis', '21013', 2, 2, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(11, 'Pan sandwich', '812393', 3, 4,1,  '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(12, 'Mogolla integral', '812394', 3, 4, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(13, 'Mogolla negra', '812395', 3, 4, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(14, 'Salsa de tomate', '64217468', 4, 6, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(15, 'Mayonesa', '64217469', 4, 6, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(16, 'Cereales niño', '64217470', 4, 6, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(17, 'Cereales adulto', '64217471', 4, 6, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23'),
(18, 'Mani saborizado', '64217472', 4, 6, 1, '2021-09-14 02:39:23', '2021-09-14 02:39:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider`
--

CREATE TABLE `provider` (
  `idprovider` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `nit` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `manager_account` varchar(45) DEFAULT NULL,
  `mobile_manager` varchar(45) DEFAULT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `provider`
--

INSERT INTO `provider` (`idprovider`, `name`, `nit`, `address`, `mobile`, `manager_account`, `mobile_manager`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Huevos santa reyes', '8600294450-1', 'AVENIDA SUBA 10 # 3B 21', '2264161', 'ESTEFANIA AROCAS PASADAS', '938205580', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(2, 'COLANTA', '8909044786', 'CALLE 74 64 A 51', '4455555', 'QUERALT VISO GILABERT', '936545115', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(3, 'DIANA CORPORACION S A S', '8600316066', 'CARRERA 13 93 24', '6231799', 'JOAN AYALA FERRERAS', '938202768', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(4, 'BIMBO DE COLOMBIA S A', '8300023660', 'AUTOPISTA MEDELLIN KM 12, TENJO', '7452111', 'JOAN BAEZ TEJADO', '938727844', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(5, 'ALIANZA TEAM', '860005813-4', 'CARRERA 11 # 84-09', '6515700', 'MARC BASTARDES SOTO', '938350521', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(6, 'Grupo Nutresa S.A.', '890900050-1', 'Carrera 43A Nº 1A Sur 143', '2689642', 'JOSEP ANGUERA VILAFRANCA', '938755645', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(7, 'POSTOBON', '8909039395', 'CALLE 52 47 42 PISO 25 ', '5765100', 'ESTHER PASCUAL ALOY', '936520547', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(8, 'UNILEVER ANDINA COLOMBIA', '8600025182', 'Av. El Dorado #69b-45', '4239700', 'LAURA VALLÉS GIRVENT', '936565656', 1, '2021-09-14 02:38:12', '2021-09-14 02:38:12'),
(9, 'Avicola nacional s.a', '890911625-1', 'VEREDA LAS GARZONAS PREDIO 146 ', '4489868', 'RAQUEL RAYA GARCIA', '936752156', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(10, 'Huevos la Sabana', '860061202-2', 'CARRERA 29 # 68 33', '6300712', 'JOAN ANDREU CRUZ', '938300025', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(11, 'Huevos oro', '860009520-1', 'Cra 109b #  145b 17', '3135503695', 'MARIA ISABEL BARALDÉS COMAS', '938385567', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(12, 'Santa Anita Napoles S A', '900211671', 'CALLE 70 NTE # 3 CN 275 BG 2', '4851919', 'ADRIÀ BERENGUERAS CULLERÉS', '937809812', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(13, 'ALQUERIA', '860004922-4', 'KM.5 VIA A TABIO', '405 6464 ', 'GERARD LÓPEZ DE PABLO GARCIA UCEDA', '936520741', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(14, 'Alpina Productos Alimenticios S A Bic', '8600259002', 'CARRERA 4 7 99 ', '4238600', 'ELIOT ARNAU MORENO', '938202456', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(15, 'COOLECHERA', '8200045430', 'CALLE 17 # 16-55', '3103170922', 'JORDI RAYA GAVILAN', '938754554', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(16, 'PARMALAT', '800245795', 'DIAGONAL 182 N. 20 - 84', '6799998', 'LLUÍS ZAMBUDIO FIGULS', '936875544', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(17, 'ARROZ SUPREMO SAS', '9012078050', 'LUGAR KILOMETRO 35 YOPAL ', '3102894245', 'LAURA BIDAULT CULLERÉS', '935880712', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(18, 'Organizacion Roa Florhuila S.a', '891100445-6', 'CALLE 18a # 69b-51', '4119999', 'JORDI BIOSCA FONTANET', '936875255', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(19, 'ARROZ SONORA', '800020220', 'CALLE 17 # 63-99A', '3176443825', 'DOUNYA ZAFRA FIGULS', '936542775', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(20, 'ARROZ FLORHUILA', '891100441-6', 'CALLE 18a # 69b-51', '4119999', 'JULIO ALEU ICART', '938773545', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(21, 'Industria Santa Clara S A S', '8600506257', 'CARRERA 43 9 46', '2442777', 'ANDREU BADIA TORNÉ', '938200022', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(22, 'COMAPAN S A', '8600002583', 'CARRERA 42 B 14 18', '3299390', 'RAMON MORALES GESE', '936512545', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(23, 'Ramo Sas', '8600038318', 'CARRERA 27 A 68 50', '4375735', 'DAVID-JESE BLANCO FONTANET', '937785655', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(24, 'Harinera Del Valle S A', '8913003829', 'CARRERA 33 A 16 04', '4187000', 'ARAN ALVAREZ FERNÁNDEZ', '938300385', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(25, 'Grasco Ltda', '8600052640', 'CARRERA 35 7 50', '4440444', 'GEMMA GARCIA ALMOGUERA', '936520471', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(26, 'COMPAÑIA NACIONAL DE CHOCOLATES S.A.S', '8110360309', 'CARRERA  65 # 12 - 60', '4173250', 'IVAN LIBORI FIGUERAS', '936012445', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(27, 'COLOMBINA S A, VALLE', '8903018845', 'CORREGIDURIA LA PAILA MCP ZARZAL', '2339200', 'DAVID BIDAULT PUEYO', '934500611', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(28, 'Quala S.A', '8600744501', 'CARRERA 68 D 39 F 51 SUR ', '4824858', 'XAVIER BENITEZ JOSE', '938350593', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(29, 'PEPSICO', '8909203041', 'CALLE 110 # 9 25 ', '4232480', 'MARIO PASCUAL FLORES', '939962045', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(30, 'Productos Yupi Sas', '8903155508', 'CALLE 15 # 32 234 ', '6518600', 'JESUS AYALA TORNÉ', '938755603', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(31, 'COCA COLA BEBIDAS COLOMBIANAS  S A ', '890903818-7', 'AVENIDA CARRERA 45 N 103 60 PISO 8', '6386600', 'GEMMA LISTAN FIGUERAS', '938305524', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(32, 'Cervecería Bavaria', '8600062246', 'Carrera 53A # 127-35', '6389000', 'SILVIA RASERO GAVILAN', '936571974', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(33, 'Poker Latinoamerica S A S', '8001304207 ', 'CALLE 96 9 A 30', '3183903506', 'ALBERT ARNALOT PUIG', '938300036', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(34, 'Quala S.A', '8600744109', 'CARRERA 68 D 39 F 51 SUR ', '4824858', 'MARIA MOLINER GARRIDO', '936505455', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(35, ' Procter & Gamble Colombia', '8000001464', 'CARRERA 7 114 33', '5208000', 'BERTA GALOBART GARCIA', '936587454', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13'),
(36, 'GRUPO FAMILIA', '8909031619', 'CARRERA 50 8 SUR 117 en la ciudad de MEDELLIN', '3609500', 'BERTA LÓPEZ GARRIGASSAIT', '938725845', 1, '2021-09-14 02:38:13', '2021-09-14 02:38:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sale`
--

CREATE TABLE `sale` (
  `idsale` int NOT NULL,
  `cliente_id` varchar(45) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `state`
--

CREATE TABLE `state` (
  `idstate` int NOT NULL,
  `state` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `state`
--

INSERT INTO `state` (`idstate`, `state`, `created_at`, `updated_at`) VALUES
(1, 'activo', '2021-09-14 02:36:44', '2021-09-14 02:36:44'),
(2, 'inactivo', '2021-09-14 02:36:44', '2021-09-14 02:36:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_product`
--

CREATE TABLE `type_product` (
  `idtype_product` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `type_product`
--

INSERT INTO `type_product` (`idtype_product`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Huevos', 1, '2021-09-14 02:38:44', '2021-09-14 02:38:44'),
(2, 'Lacteos', 1, '2021-09-14 02:38:53', '2021-09-14 02:38:53'),
(3, 'Pan', 1, '2021-09-14 02:39:02', '2021-09-14 02:39:02'),
(4, 'Abarrotes', 1, '2021-09-14 02:39:12', '2021-09-14 02:39:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_user`
--

CREATE TABLE `type_user` (
  `idtype_user` int NOT NULL,
  `type_user` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `type_user`
--

INSERT INTO `type_user` (`idtype_user`, `type_user`, `status`, `created_at`, `updated_at`) VALUES
(1, 'administrador', 1, '2021-09-16 02:24:15', '2021-09-16 02:24:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `iduser` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `mobile` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `document_type` int NOT NULL,
  `identification` varchar(45) NOT NULL,
  `password` varchar(8) DEFAULT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`iduser`, `name`, `lastname`, `mobile`, `email`, `document_type`, `identification`, `password`,`status`, `created_at`, `updated_at`) VALUES
(2, 'Mauricio', 'Suarez', '3114541680', 'mauriciosuarezcalderon@gmail.com', 1, '79729511', '123456', 1,'2021-09-16 02:24:49', '2021-09-16 02:24:49');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idclient`),
  ADD UNIQUE KEY `identification_UNIQUE` (`identification`),
  ADD UNIQUE KEY `idclient_UNIQUE` (`idclient`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `mobile_UNIQUE` (`mobile`),

--
-- Indices de la tabla `details_sale`
--
ALTER TABLE `details_sale`
  ADD PRIMARY KEY (`iddetails_sale`),
  ADD UNIQUE KEY `iddetails_sale_UNIQUE` (`iddetails_sale`);

--
-- Indices de la tabla `document_type`
--
ALTER TABLE `document_type`
  ADD PRIMARY KEY (`idtable1`),
  ADD UNIQUE KEY `idtable1_UNIQUE` (`idtable1`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`idproduct`),
  ADD UNIQUE KEY `code_UNIQUE` (`code`),
  ADD UNIQUE KEY `idproduct_UNIQUE` (`idproduct`);

--
-- Indices de la tabla `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`idprovider`),
  ADD UNIQUE KEY `nit_UNIQUE` (`nit`),
  ADD UNIQUE KEY `idprovider_UNIQUE` (`idprovider`);

--
-- Indices de la tabla `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`idsale`);

--
-- Indices de la tabla `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`idstate`),
  ADD UNIQUE KEY `idstate_UNIQUE` (`idstate`);

--
-- Indices de la tabla `type_product`
--
ALTER TABLE `type_product`
  ADD PRIMARY KEY (`idtype_product`),
  ADD UNIQUE KEY `idtype_product_UNIQUE` (`idtype_product`);

--
-- Indices de la tabla `type_user`
--
ALTER TABLE `type_user`
  ADD PRIMARY KEY (`idtype_user`),
  ADD UNIQUE KEY `idtype_user_UNIQUE` (`idtype_user`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `identification_UNIQUE` (`identification`),
  ADD UNIQUE KEY `iduser_UNIQUE` (`iduser`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `mobile_UNIQUE` (`mobile`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `idclient` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `details_sale`
--
ALTER TABLE `details_sale`
  MODIFY `iddetails_sale` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `document_type`
--
ALTER TABLE `document_type`
  MODIFY `idtable1` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `idproduct` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `provider`
--
ALTER TABLE `provider`
  MODIFY `idprovider` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `state`
--
ALTER TABLE `state`
  MODIFY `idstate` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `type_product`
--
ALTER TABLE `type_product`
  MODIFY `idtype_product` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `type_user`
--
ALTER TABLE `type_user`
  MODIFY `idtype_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
